public class Human {
    private String name;
    private double money;
    private double hunger;
    private double boredom;

    public Human(String name)
    {
        this.name = name;
        this.money = 0;
        this.hunger = 0;
        this.boredom = 0;
    }

    public boolean doWork() 
    {
        if (this.hunger > 50)
        {
            System.out.println("Human is too hungry to work.");
            return false;
        }
        else if (this.boredom > 50)
        {
            System.out.println("Human is too bored to work.");
            return false;
        }
        else
        {
            this.money += 20;
            this.hunger += 5;
            this.boredom += 10;

            System.out.println("Human has successfully done work.");

            return true;
        }
    }

    public void play(){
        if(this.boredom <= 30){
            this.boredom = this.boredom - 30;
        }
        else{
            this.boredom = 0;
        }
        this.hunger = this.hunger-5;
        System.out.println("The human played with the dog.");
    public void getScratched()
    {
        this.boredom -= 30;
        System.out.println(name+" has been scratched by an evil cat.");
    }
}
