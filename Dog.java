public class Dog{
    private String name;
    private int energy;
    private int hunger;
    private int boredom;
    
    public Dog(String name){
        this.name = name;
        this.energy = 0;
        this.hunger = 0;
        this.boredom = 0;
    }

    public boolean takeNap(){
        if(this.hunger >= 50){
            System.out.println("The dog is too hungry.");
            return false;
        }
        else if(this.boredom >= 50){
            System.out.println("The dog is too bored.");
            return false;
        }
        else{
            this.energy += 20;
            this.hunger += 5;
            this.boredom += 10;
            System.out.println("The dog has rested succesfully!");
            return true;
        }
    }
    public void play(){
        if(this.boredom <= 30){
            this.boredom = this.boredom - 30;
        }
        else{
            this.boredom = 0;
        }
        this.hunger = this.hunger-5;
        System.out.println("The dog played with the human.");
    }

    public boolean playWith(Human man){
        if(this.energy > 50){
            this.energy = this.energy - 50;
            play();
            man.play();
            return true;
        }
        else return false;
    }
}